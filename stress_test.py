#! /usr/bin/python
"""
Stress test simulator

For more information: python stress_test.py --help
"""

import logging
import click
from scalp import Network, random, statistics, perf
from scalp.router import unicast
from scalp.node.computingcore import stresstest
from scalp.node import distribution
from scalp.common import PositionVector, BroadcastMessage, ReduceOperator, ReduceMessage

logging.basicConfig(level=logging.INFO)


def configure_interfaces():
    # throughput defined as fraction of message send per cycle (supposed message size: 32bytes)
    # latency defined in cycles
    fast_interface = perf.NIFConfig(throughput=1 / 32, latency=50)
    low_latency_interface = perf.NIFConfig(
        throughput=1 / 64, latency=10
    )  # half the throughput, a fifth of the latency
    noc_interface = perf.NIFConfig(
        throughput=0, latency=0
    )  # FPGA<->ComputingCore local communication

    perf.set_route_cost(5)  # cost in cycles for 1 routing operation
    perf.set_interface(perf.Dimension.X, fast_interface)
    perf.set_interface(perf.Dimension.Y, fast_interface)
    perf.set_interface(perf.Dimension.Z, fast_interface)
    perf.set_interface(perf.Dimension.NOC, noc_interface)


routers = {
    "xyz": unicast.XyzRouter,
    "xyz_adapt": unicast.XyzAdaptiveRouter,
    "adaptive": unicast.AdaptiveRouter,
    "adaptive_vc": unicast.VirtualChannelRouter,
}

computingcores = {
    "unicast": stresstest.GenerateUnicastMessages,
    "broadcast": stresstest.GenerateBroadcastMessages,
    "reduce": stresstest.GenerateReduceMessages,
}

distributions = {
    "uniform": distribution.uniform,
    "complement": distribution.complement,
    "digitrevert": distribution.digitrevert,
    "sphere_of_locality": distribution.sphere_of_locality(3 ** (1 / 2), 0.9),
}


@click.command()
@click.option(
    "--computingcore", required=True, type=click.Choice(["broadcast", "unicast", "reduce"])
)
@click.option(
    "--router",
    required=True,
    type=click.Choice(["xyz", "adaptive", "xyz_adapt", "adaptive_vc"]),
)
@click.option(
    "--dist",
    type=click.Choice(["uniform", "complement", "digitrevert", "sphere_of_locality"]),
)
@click.option("--rate", type=click.FloatRange(1e-9, 2 ** 32))
@click.option("--seed", type=click.IntRange(1, 2 ** 32))
@click.option("--side", default=4)
@click.option("--timeout", default=15)
def main(computingcore, router, dist, rate, seed, side, timeout):
    random.seed(seed)
    computingcoretype = computingcores[computingcore]
    if rate:
        computingcoretype.INTERVAL = side ** 3 / rate
    if dist:
        computingcoretype.DISTRIBUTION = distributions[dist]

    network = Network(
        size=PositionVector(side, side, side),
        router_type=routers[router],
        computingcore_type=computingcoretype,
    )

    network.simulate()

    ended_ok = network.wait_end(timeout)
    if ended_ok:
        print(", ".join(map(str, [rate, *statistics.get_stats().values()])))
        # statistics.print_stats()
        # statistics.print_queues_state()
        # statistics.display_received_stats()
    else:
        statistics.print_error()


if __name__ == "__main__":
    configure_interfaces()
    main()
