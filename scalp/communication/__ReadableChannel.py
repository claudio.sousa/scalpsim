from abc import ABC, abstractmethod
from typing import Deque, Optional
from threading import Event
from collections import deque

from scalp.common import Message, Direction, PositionVector
from scalp import statistics, perf
from scalp.perf.Clock import Clock


class ReadableChannel(ABC):

    @abstractmethod
    def pop(self) -> Message:
        pass

    @abstractmethod
    def peek(self) -> Optional[Message]:
        pass

    @abstractmethod
    def size(self) -> int:
        pass

    @abstractmethod
    def push(self, msg: Message):
        pass

    @abstractmethod
    def free_space(self) -> float:
        pass
