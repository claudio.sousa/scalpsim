from typing import Deque, Optional, ClassVar
from threading import Event
from collections import deque

from scalp.common import Message, Direction, PositionVector
from scalp import statistics, perf
from scalp.perf.Clock import Clock
from .__ReadableChannel import ReadableChannel


class VcChannel(ReadableChannel):
    BUFFER_SIZE: ClassVar[int] = 32

    def __init__(self, clock: Clock) -> None:
        self.__queue: Deque[Message] = deque()
        self.__clock = clock

    def pop(self) -> Message:
        return self.__queue.pop()

    def __update_msg_delay(self, msg: Message) -> None:
        delta_time = self.__clock.time - msg.stats.last_update_time
        msg.stats.transport_time += delta_time
        msg.stats.current_hop_time += delta_time
        msg.stats.max_hop_time = max(msg.stats.max_hop_time, msg.stats.current_hop_time)
        msg.stats.last_update_time = self.__clock.time

    def peek(self) -> Optional[Message]:
        if not self.__queue:
            return None
        msg = self.__queue[-1]
        self.__update_msg_delay(msg)
        return msg

    def size(self) -> int:
        return len(self.__queue)

    def push(self, msg: Message):
        self.__queue.appendleft(msg)

    def free_space(self) -> float:
        """
        Free space left, in [0, 1]. 0->full, 1->empty
        """
        return 1 - self.size() / VcChannel.BUFFER_SIZE
