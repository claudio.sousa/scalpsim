from scalp import perf


class Clock:
    def __init__(self) -> None:
        self.__time: int = 0

    @property
    def time(self) -> int:
        return self.__time

    def route_done(self) -> None:
        self.__time += perf.route_cost()
