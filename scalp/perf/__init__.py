from .__NIFConfig import Dimension, NIFConfig
from scalp.common import Direction

__DIR_TO_DIM = {
    Direction.NORTH: Dimension.Y,
    Direction.SOUTH: Dimension.Y,
    Direction.EAST: Dimension.X,
    Direction.WEST: Dimension.X,
    Direction.UP: Dimension.Z,
    Direction.DOWN: Dimension.Z,
    Direction.ComputingCore: Dimension.NOC,
    Direction.FPGA: Dimension.NOC,
}


class __PerfConfig:
    def __init__(self):
        self.route_cost: int = 0
        self.interface_config: Dict[Dimension, NIFConfig] = {}


__perfConfig = __PerfConfig()


def set_route_cost(route_cost: int) -> None:
    """
    Defines the cost, in cycles, for 1 routing operation
    """
    __perfConfig.route_cost = route_cost


def route_cost() -> int:
    return __perfConfig.route_cost


def set_interface(dimension: Dimension, config: NIFConfig) -> None:
    __perfConfig.interface_config[dimension] = config


def get_interface(direction: Direction) -> NIFConfig:
    assert (
        direction in __DIR_TO_DIM
    ), f"Direction {direction} is not mapped to a dimension"
    dimension = __DIR_TO_DIM[direction]

    assert (
        dimension in __perfConfig.interface_config
    ), f"Dimension {dimension} has not been configured"
    return __perfConfig.interface_config[dimension]
