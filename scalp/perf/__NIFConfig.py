from typing import Dict
from enum import Enum


class Dimension(Enum):
    X = 0
    Y = 1
    Z = 2
    NOC = 3


class NIFConfig:
    """
    Network interface performance properties
    """

    def __init__(self, throughput: float, latency: int) -> None:
        """
        :param: throughput Defines throughput as fraction of message send per cycle
        :param: latency Defines latency in cycles
        """

        self.throughput = throughput
        self.latency = latency

