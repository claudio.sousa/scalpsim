from itertools import product
from typing import List, Dict, Tuple, Optional, Iterable, Type
from threading import Event
from time import sleep
from scalp import random
from scalp.node import Node
from scalp.common import (
    PositionVector,
    directions_between_neighbours,
    neighbour_positions,
)
from scalp.communication import PhysicalNode
from scalp.perf.Clock import Clock
from scalp.router import Router, unicast
from scalp.node.computingcore import ComputingCore
from scalp import statistics


class Network:
    def __init__(
        self,
        size: PositionVector,
        computingcore_type: Type[ComputingCore],
        router_type: Type[Router] = unicast.XyzRouter,
    ) -> None:
        self.__size = size
        self.__rng = random.create_random_state()
        self.__unicast_router_type = router_type
        self.__computingcore_type = computingcore_type
        self.__start_event = Event()
        self.__build_network()

    @property
    def size(self) -> PositionVector:
        return self.__size

    @property
    def nodes(self) -> Dict[PositionVector, Node]:
        return self.__nodes

    @property
    def start_event(self) -> Event:
        return self.__start_event

    def __build_network(self) -> None:
        self.__nodes_physicalnode: Dict[PositionVector, Tuple[PhysicalNode, Clock]] = {}
        for pos in product(*[range(d) for d in self.__size]):
            position = PositionVector(*pos)
            clock = Clock()
            self.__nodes_physicalnode[position] = (PhysicalNode(position, clock), clock)

        for pos, (phys, clock) in self.__nodes_physicalnode.items():
            neighbours = self.get_neighbours(pos)
            for neighbour_pos in neighbours:
                directions = directions_between_neighbours(pos, neighbour_pos)
                assert (
                    directions is not None
                ), "Cannot find direction between neighbours"
                diemention, send_dir, received_dir = directions
                neighbour_channel = self.__nodes_physicalnode[neighbour_pos][0].in_channels[
                    received_dir
                ]
                phys.attach_neighbour_channel(send_dir, neighbour_channel)

        self.__nodes: Dict[PositionVector, Node] = {}

        for pos, (physicalnode, clock) in self.__nodes_physicalnode.items():
            self.__nodes[pos] = Node(
                position=pos,
                network_size=self.__size,
                physicalnode=physicalnode,
                clock=clock,
                start_event=self.__start_event,
                router_type=self.__unicast_router_type,
                computingcore_type=self.__computingcore_type,
            )

    def get_neighbours(self, pos: PositionVector) -> Iterable[PositionVector]:
        for possible_pos in neighbour_positions(pos):
            if possible_pos in self.__nodes_physicalnode:
                yield possible_pos

    def simulate(self) -> None:
        statistics.reset_simulation(size=self.__size)
        statistics.started()

        self.__start_event.set()  # awakes those waiting for the start event
        sleep(0.1)

    def wait_end(self, timeout: int = None) -> bool:
        statistics.finished()
        return statistics.wait_end(timeout)
