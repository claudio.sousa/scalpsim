from threading import Thread
from scalp import random
from time import sleep
from scalp import Network
from scalp import statistics
from scalp.common import UnicastMessage, PositionVector, Direction


class RateDataGenerator:
    def __init__(self, communication: Network, interval_ms: float = None, thread_count:int=1) -> None:
        self.__interval_ms = interval_ms
        self.__thread_count = thread_count
        self.__network = communication
        self.__rng = random.create_random_state()

    def __pick_random_position(self, different_from: PositionVector = None):
        rnd_pos = different_from
        while rnd_pos == different_from:
            rnd_pos = PositionVector(
                *[self.__rng.randint(0, dim_size) for dim_size in self.__network.size]
            )
        return rnd_pos

    def generate(self, interval_ms: float = 0) -> None:
        statistics.started()

        for i in range(self.__thread_count):
            thread = Thread(target=self.__generate_data,name=f'RateDataGenerator {i}')
            thread.daemon = True
            thread.start()

    def __generate_data(self):
        self.__network.start_event.wait()
        try:
            i = 0
            while True:
                from_pos = self.__pick_random_position()
                to_pos = self.__pick_random_position(from_pos)
                from_node = self.__network.nodes[from_pos]
                msg = UnicastMessage(from_pos=from_pos, to_pos=to_pos, payload=f"{i}")
                i += 1

                if not from_node.physicalnode.is_full(Direction.FPGA):
                    from_node.physicalnode.send(Direction.FPGA, msg)
                if self.__interval_ms is not None:
                    sleep(self.__interval_ms*self.__thread_count)
        except:
            statistics.error()
            raise
        statistics.finished()
