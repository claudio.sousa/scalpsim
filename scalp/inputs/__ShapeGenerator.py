import numpy as np
from typing import List, Tuple, Iterator as TypeIterator
from collections.abc import Iterator

class ShapeGenerator(Iterator):

    def __init__(self, bounds:List[Tuple[int, int]]=[(0, 1), (0, 1)]):
        self.bounds = bounds
        self.dimensions = len(bounds)

    def __next__(self) -> TypeIterator:
        val = np.random.rand(1, self.dimensions)[0]
        for i, bound in enumerate(self.bounds):
            val[i] *= abs(bound[1] - bound[0])
            val[i] += bound[0]
        return val
