
import numpy as np
from typing import Iterator as TypeIterator
from collections.abc import Iterator

class RandomGenerator(Iterator):

    def __init__(self, low: int, high: int, dimensions=1):
        self.low = low
        self.high = high
        self.dimensions = dimensions

    def __next__(self) -> TypeIterator:
        return np.random.uniform(low=self.low, high=self.high, size=self.dimensions)
