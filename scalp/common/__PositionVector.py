
from typing import NamedTuple


class PositionVector(NamedTuple):
    x: int
    y: int
    z: int