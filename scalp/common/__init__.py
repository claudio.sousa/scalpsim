from .__PositionVector import PositionVector
from .__Direction import Direction, directions_between_neighbours, neighbour_positions
from .__Message import Message, UnicastMessage, ReduceMessage, ReduceResponseMessage, BroadcastMessage, ReduceOperator
from time import sleep

# Note using 0 for epsilon causes performance issues (tested on a debian)
YIELD_EPSILON = 0.0001

def thread_yield():
    sleep(YIELD_EPSILON)
