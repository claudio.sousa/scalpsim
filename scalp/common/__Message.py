from abc import ABC, abstractmethod
from enum import Enum, auto
from uuid import UUID, uuid4
from typing import Any, Type, Dict, Callable, Union
from scalp.common import PositionVector


class MsgStats:
    def __init__(self):
        self.hops: int = 0
        self.transport_time: int = 0
        self.sent_time: int = 0
        self.last_update_time: int = 0
        self.max_hop_time: int = 0
        self.current_hop_time: int = 0


class Message(ABC):
    def __init__(self) -> None:
        super().__init__()
        self.stats = MsgStats()


class UnicastMessage(Message):
    def __init__(
        self, source: PositionVector, target: PositionVector, payload: Any
    ) -> None:
        super().__init__()
        self.source = source
        self.target = target
        self.payload = payload

    def __str__(self) -> str:
        return f"UnicastMessage {self.source}-> {self.target}"


class MulticastMessage(Message, ABC):
    pass


class BroadcastMessage(MulticastMessage):
    def __init__(self, source: PositionVector, payload: Any) -> None:
        super().__init__()
        self.payload = payload
        self.source = source

    def duplicate(self) -> "BroadcastMessage":
        return BroadcastMessage(source=self.source, payload=self.payload)

    def __str__(self) -> str:
        return f"BroadcastMessage {self.source} - {self.payload}"


class ReduceOperator(Enum):
    MIN = auto()  # min value and the it's process location
    MAX = auto()  # maximum value and the it's process location
    SUM = auto()  # Sum of all elements
    PROD = auto()  # Product of all values
    AND = auto()  # Bitwise AND between all values
    OR = auto()  # Bitwise OR between all values


REDUCE_FN_BY_OP: Dict[ReduceOperator, Callable[[Union[float, int], Union[float, int]], Union[float, int]]] = {
    ReduceOperator.MIN: min,
    ReduceOperator.MAX: max,
    ReduceOperator.SUM: lambda a, b: a + b,
    ReduceOperator.PROD: lambda a, b: a * b,
    ReduceOperator.AND: lambda a, b: a & b,
    ReduceOperator.OR: lambda a, b: a | b,
}


class ReduceResponseMessage(MulticastMessage):
    def __init__(
        self,
        target: PositionVector,
        operator: ReduceOperator,
        value: Any,
        uuid: UUID,
        source: PositionVector = None,
    ) -> None:
        super().__init__()
        self.target = target
        self.uuid = uuid
        self.operator = operator
        self.value = value
        self.source = source

    def combine(self, reduce_msg: "ReduceResponseMessage") -> None:
        """
        Combine a reduce response into the current one
        """

        combine_fn = REDUCE_FN_BY_OP[self.operator]
        self.value = combine_fn(self.value, reduce_msg.value)
        if (
            self.operator in (ReduceOperator.MAX, ReduceOperator.MIN)
            and self.value == reduce_msg.value
        ):
            self.source = reduce_msg.source

    def __str__(self) -> str:
        return f"ReduceResponseMessage {self.value}@{self.source}"


class ReduceMessage(BroadcastMessage):
    def __init__(
        self,
        source: PositionVector,
        operator: ReduceOperator,
        payload: Any = None,
        uuid: UUID = None,
    ) -> None:
        super().__init__(source, payload)
        self.operator = operator
        self.uuid = uuid if uuid else uuid4()

    def duplicate(self) -> "ReduceMessage":
        return ReduceMessage(
            source=self.source,
            operator=self.operator,
            payload=self.payload,
            uuid=self.uuid,
        )

    def create_response(
        self, value: Any, source: PositionVector
    ) -> ReduceResponseMessage:
        return ReduceResponseMessage(
            target=self.source,
            operator=self.operator,
            value=value,
            uuid=self.uuid,
            source=source,
        )

