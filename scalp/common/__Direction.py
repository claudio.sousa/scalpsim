from enum import Enum
from typing import Tuple, Dict, List, Optional, Iterable
from .__PositionVector import PositionVector


def direction_value(dimension: int, increasing: bool) -> int:
    return (1 << (dimension + 1)) + increasing


class Direction(Enum):
    EAST = direction_value(0, True)
    WEST = direction_value(0, False)
    NORTH = direction_value(1, True)
    SOUTH = direction_value(1, False)
    UP = direction_value(2, True)
    DOWN = direction_value(2, False)
    ComputingCore = direction_value(3, True)
    FPGA = direction_value(3, False)

    @staticmethod
    def from_value(value: int):
        return Direction._value2member_map_[value]  # type: ignore

    @staticmethod
    def from_dimension(dimension: int, increasing: bool):
        return Direction.from_value(direction_value(dimension, increasing))

    @property
    def dimension(self) -> int:
        return self.value >> 1

    @property
    def increasing(self) -> bool:
        return bool(self.value & 1)


def directions_between_neighbours(
    n1: PositionVector, n2: PositionVector
) -> Tuple[int, Direction, Direction]:
    """
    Retrieves the directions between two connected neighbours
    """
    for dim in range(len(n1)):
        if n1[dim] != n2[dim]:
            dir_val = direction_value(dim, n1[dim] < n2[dim])
            return dim, Direction.from_value(dir_val), Direction.from_value(dir_val ^ 1)

    raise Exception(f"Not direct neighbours: {n1} and {n2}")


def neighbour_positions(pos: PositionVector) -> Iterable[PositionVector]:
    for dim in range(len(pos)):
        temp_pos = list(pos)
        temp_pos[dim] -= 1
        yield PositionVector(*temp_pos)
        temp_pos[dim] += 2
        yield PositionVector(*temp_pos)
