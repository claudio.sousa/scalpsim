from typing import Optional, List
from scalp.common import Direction, UnicastMessage
from scalp import statistics
from scalp.router import RouteResult
from scalp.communication import InChannel
from .__UnicastRouter import UnicastRouter


class XyzAdaptiveRouter(UnicastRouter):

    def _route_unicast_msg(self, msg: UnicastMessage) -> RouteResult:
        route_res = RouteResult.NO_MSG_TO_ROUTE
        for dim in range(len(self._position)):
            if self._position[dim] != msg.target[dim]:
                direction = Direction.from_dimension(
                    dim, self._position[dim] < msg.target[dim]
                )

                if self._phys.is_full(direction):
                    route_res = RouteResult.FULL_OUTGOING_CHANNELS
                    continue

                self._phys.send(direction, msg)
                return RouteResult.MSG_ROUTED

        return route_res
