from typing import Optional, List
from scalp.common import Direction, UnicastMessage
from scalp.communication import InChannel
from scalp import statistics
from scalp.router import RouteResult
from .__UnicastRouter import UnicastRouter


class XyzRouter(UnicastRouter):
    """
    Also known as 'dimension-order routing', this algorithm routes messages in
    in strictly increasing  dimensions.
    The message is first routed in the X dimension until it reaches the a node in the same X
    corrdinate as the target. It then routes on the Y dimension until it reaches a node in the Y dimension,
    and only then routes on the Z dimension
    This algorithm produces a determinist path for each tuple of starting and target positions.
    """

    def _route_unicast_msg(self, msg: UnicastMessage) -> RouteResult:
        for dim in range(len(self._position)):
            if self._position[dim] != msg.target[dim]:
                direction = Direction.from_dimension(
                    dim, self._position[dim] < msg.target[dim]
                )

                if self._phys.is_full(direction):
                    return RouteResult.FULL_OUTGOING_CHANNELS

                self._phys.send(direction, msg)
                return RouteResult.MSG_ROUTED

        return RouteResult.NO_MSG_TO_ROUTE

