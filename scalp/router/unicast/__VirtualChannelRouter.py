from abc import ABC, abstractmethod
from typing import Optional, List, Tuple, Dict
from itertools import product
from collections import defaultdict
import numpy as np

from scalp import random, statistics
from scalp.common import Direction, Message, PositionVector, UnicastMessage
from scalp.communication import PhysicalNode, InChannel
from scalp.communication import VcChannel
from scalp.router import Router, RouteResult
from ..__RouteResult import RouteResult


class VirtualChannelRouter(Router):
    """
    Based on the XYZ router ('dimension-order routing'), this routes using the first available
    dimension according to channels congestion.
    The paths generated are no longer deterministic, but they remain acyclic.
    Thus it remains deadlock free.
    """

    def __init__(self, phys: PhysicalNode, position: PositionVector):
        super().__init__(phys, position)
        self.__build_vcs()

    def __build_vcs(self):
        """
        offset_signs is a 3 digit tuple
        each digit means the offset sign in one dimension
        if the bit is 0, it means that

          X  Y  Z
        ( 0, 0, 0) -> No dimension offset we are at target. Destined at the ComputingCore channel
        ( 0, 0, 1) -> Target as positive Z dimension offset exclusively
        ( 0, 1,-1) -> Target as negative Z dimension offset and positive Y dimension offset

        ...
        """

        self._vcs_by_direction: Dict[Direction, List[VcChannel]] = defaultdict(list)
        self._vcs_by_offset_sign: Dict[Tuple[int, int, int], VcChannel] = {}
        self._directions_by_vc: Dict[VcChannel, List[Direction]] = defaultdict(list)

        for offsets_sign in list(product(*[(0, 1, -1)] * 3)):
            vc = VcChannel(self._phys.clock)
            self._vcs_by_offset_sign[offsets_sign] = vc

            if offsets_sign == (0, 0, 0):
                self._directions_by_vc[vc].append(Direction.ComputingCore)
                self._vcs_by_direction[Direction.ComputingCore].append(vc)
            else:
                for dim in range(3):
                    offset = offsets_sign[dim]
                    if offset != 0:
                        direction = Direction.from_dimension(dim, offset > 0)
                        self._vcs_by_direction[direction].append(vc)
                        self._directions_by_vc[vc].append(direction)

    def __get_target_vc(self, msg: UnicastMessage) -> VcChannel:
        offsets_sign = [np.sign(p2 - p1) for p1, p2 in zip(self._position, msg.target)]
        return self._vcs_by_offset_sign[tuple(offsets_sign)]

    def _route_channel_to_vcs(self, channel: VcChannel):
        msg = channel.peek()
        while msg and isinstance(msg, UnicastMessage):
            vc = self.__get_target_vc(msg)
            if vc.free_space() == 0:
                return
            vc.push(msg)
            channel.pop()
            msg = channel.peek()

    def _route_to_vcs(self):
        routable_channels = [
            channel
            for (direction, channel) in self._phys.in_channels.items()
            if direction != Direction.ComputingCore and channel.free_space() < 1
        ]

        sorted_channels = sorted(
            routable_channels, key=lambda channel: channel.free_space()
        )

        for channel in sorted_channels:
            self._route_channel_to_vcs(channel)

    def _get_sorted_vcs(self) -> List[VcChannel]:
        non_empty_channels = [
            channel
            for (offset, channel) in self._vcs_by_offset_sign.items()
            if offset != (0, 0, 0) and channel.free_space() < 1
        ]
        # we favor the internal channel
        sorted_channels = sorted(
            non_empty_channels, key=lambda channel: channel.free_space()
        )

        computingcore_channel = self._vcs_by_direction[Direction.ComputingCore][0]
        if computingcore_channel.free_space() < 1:
            sorted_channels = [computingcore_channel] + sorted_channels
        return sorted_channels

    def _get_sorted_directions(self, directions: List[Direction]) -> List[Direction]:
        return sorted(
            (
                direction
                for direction in directions
                if not self._phys.is_full(direction)
            ),
            key=lambda direction: self._phys.free_space(direction),
            reverse=True,
        )

    def _route_from_vcs(self, vcs: List[VcChannel]) -> RouteResult:

        found_msg = False
        for vc in vcs:
            if vc.free_space() == 1:
                continue
            found_msg = True
            msg = vc.peek()

            if not msg:
                continue
            directions = self._get_sorted_directions(self._directions_by_vc[vc])
            for direction in directions:
                if self._phys.is_full(direction):
                    continue
                self._phys.send(direction, msg)
                vc.pop()
                statistics.routed(self._position)
                return RouteResult.MSG_ROUTED

        return (
            RouteResult.FULL_OUTGOING_CHANNELS
            if found_msg
            else RouteResult.NO_MSG_TO_ROUTE
        )

    def perform_route(self) -> RouteResult:
        """
        Routes the message in the first dimension (XYZ) with offset
        :returns: If there might be unrouted elements
        """
        self._route_to_vcs()
        vcs = self._get_sorted_vcs()
        return self._route_from_vcs(vcs)

    def __str__(self):
        return f"Router {self._position}"
