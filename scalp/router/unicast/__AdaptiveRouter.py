from typing import Optional, List
from scalp import statistics
from scalp.common import Direction, UnicastMessage
from scalp.router import RouteResult
from scalp.communication import InChannel
from .__UnicastRouter import UnicastRouter


class AdaptiveRouter(UnicastRouter):
    def _route_unicast_msg(self, msg: UnicastMessage) -> RouteResult:
        deltas = [p2 - p1 for p1, p2 in zip(self._position, msg.target)]

        directions = [
            Direction.from_dimension(dim, delta > 0)
            for dim, delta in enumerate(deltas)
            if delta != 0
        ]

        free_spaces = [self._phys.free_space(direction) for direction in directions]

        sum_p = sum(free_spaces)
        if sum_p == 0:
            statistics.interface_full(self._position)
            return RouteResult.FULL_OUTGOING_CHANNELS

        norm_free_spaces = [v / sum_p for v in free_spaces]
        direction = self._rng.choice(a=directions, p=norm_free_spaces)
        self._phys.send(direction, msg)
        return RouteResult.MSG_ROUTED
