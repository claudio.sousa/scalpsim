from abc import abstractmethod
from typing import List, Tuple, Dict
from uuid import UUID
from scalp import random, statistics
from scalp.common import Direction, Message, UnicastMessage, PositionVector
from scalp.communication import PhysicalNode, InChannel, VcChannel, ReadableChannel
from scalp.router import Router, RouteResult


class UnicastRouter(Router):
    def __init__(self, phys: PhysicalNode, position: PositionVector):
        super().__init__(phys, position)

    def __sorted_channels(self) -> List[InChannel]:
        """
        Gets all routable channels, sorted by occupation DESC
        """
        routable_channels = [
            channel
            for (direction, channel) in self._phys.in_channels.items()
            if direction != Direction.ComputingCore and channel.free_space() < 1
        ]
        return sorted(routable_channels, key=lambda channel: channel.free_space())

    def perform_route(self) -> RouteResult:
        """
        Performs routing operations in the following order:
            1. internal (->ComputingCore)
            2. unicast
        """

        channels = self.__sorted_channels()

        if not channels:
            # no pending msg
            return RouteResult.NO_MSG_TO_ROUTE

        route_res = self._route_internal(channels)
        route_res = route_res.merge(self._route_unicast(channels))

        return route_res

    def _route_internal(self, channels: List[InChannel]) -> RouteResult:
        """
        Checks for msg addressed at the current node at the top of an incomming channel
        """
        if self._phys.is_full(Direction.ComputingCore):
            return RouteResult.FULL_OUTGOING_CHANNELS

        for channel in channels:
            msg = channel.peek()
            if msg and isinstance(msg, UnicastMessage) and msg.target == self._position:
                self._phys.send(Direction.ComputingCore, msg)
                channel.pop()
                return RouteResult.MSG_ROUTED
        return RouteResult.NO_MSG_TO_ROUTE

    def _route_unicast(self, channels: List[InChannel]) -> RouteResult:
        """
        Checks for multicast messages at the top of a buffer
        """
        for channel in channels:
            msg = channel.peek()
            if msg and isinstance(msg, UnicastMessage):
                res = self._route_unicast_msg(msg)
                statistics.routed(self._position)
                channel.pop()
                return res

        return RouteResult.NO_MSG_TO_ROUTE

    @abstractmethod
    def _route_unicast_msg(self, msg: UnicastMessage) -> RouteResult:
        """
        Try routing unicast message
        """
        pass

    def __str__(self):
        return f"Router {self._position}"
