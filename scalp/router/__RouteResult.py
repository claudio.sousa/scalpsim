from typing import Type
from enum import Enum, auto


class RouteResult(Enum):
    NO_MSG_TO_ROUTE = 1
    FULL_OUTGOING_CHANNELS = 2
    MSG_ROUTED = 3

    def merge(self, other: "RouteResult") -> "RouteResult":
        return RouteResult._value2member_map_[max(self.value, other.value)]
