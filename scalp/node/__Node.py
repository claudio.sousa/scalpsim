from typing import Tuple, Dict, List, Optional, Type, ClassVar
from threading import Thread, Event
from scalp.common import Direction, PositionVector, Message, thread_yield
from scalp.communication import PhysicalNode
from scalp import statistics
from scalp.router import Router, RouteResult
from scalp.router.multicast import BroadcastRouter, ReduceRouter
from scalp.perf.Clock import Clock
from time import sleep


class Node:
    MAX_WAIT_TIME: ClassVar[float] = 0.05

    def __init__(
        self,
        position: PositionVector,
        network_size: PositionVector,
        physicalnode: PhysicalNode,
        start_event: Event,
        router_type: Type[Router],
        computingcore_type: Type["ComputingCore"],
        clock: Clock,
    ) -> None:
        super().__init__()

        self.__position = position
        self.__physicalnode = physicalnode
        self.__clock = clock
        self.__routers : List[Router] = [
            router_type(physicalnode, position),
            BroadcastRouter(physicalnode, position),
            ReduceRouter(physicalnode, position),
        ]

        self.__thread = Thread(target=self.__run, name=f"Node {position}")
        self.__thread.daemon = True
        self.__thread.start()

        self.__computingcore = computingcore_type(
            node=self, network_size=network_size, start_event=start_event
        )

    @property
    def physicalnode(self) -> PhysicalNode:
        return self.__physicalnode

    @property
    def position(self) -> PositionVector:
        return self.__position

    @property
    def computingcore(self) -> "ComputingCore":
        return self.__computingcore

    @property
    def clock(self) -> Clock:
        return self.__clock

    def send_msg_to_fpga(self, msg: Message) -> bool:
        if self.physicalnode.is_full(Direction.FPGA):
            return False

        msg.stats.sent_time = self.__clock.time
        self.physicalnode.send(Direction.FPGA, msg)
        statistics.injected_msg(self.__position, self.clock.time, msg)
        return True

    def __run(self) -> None:
        try:
            route_res = RouteResult.NO_MSG_TO_ROUTE
            while True:
                if route_res == RouteResult.NO_MSG_TO_ROUTE:
                    self.__physicalnode.wait_msg(Node.MAX_WAIT_TIME)

                route_res = RouteResult.NO_MSG_TO_ROUTE
                for router in self.__routers:
                    res = router.perform_route()
                    route_res = route_res.merge(res)
                    if route_res == RouteResult.MSG_ROUTED:
                        break
                self.__handle_internal_msgs()
                if route_res == RouteResult.FULL_OUTGOING_CHANNELS:
                    statistics.interface_full(self.__position)
                    thread_yield()
                self.__clock.route_done()
        except:
            statistics.error()
            raise

    def __handle_internal_msgs(self):
        while not self.__physicalnode.is_empty(Direction.ComputingCore):
            msg = self.__physicalnode.in_channels[Direction.ComputingCore].pop()
            self.__computingcore.handle_internal_msg(msg)

    def __str__(self):
        return str(self.__position)

    def __del__(self):
        print(f"{self.__position} died")
