import math
from typing import ClassVar, Tuple, Union, List, Dict, Optional, Callable
from threading import Event
from time import sleep
from functools import reduce
import numpy as np
from uuid import UUID
from dataclasses import dataclass
from scalp import statistics
from scalp.common import (
    PositionVector,
    UnicastMessage,
    BroadcastMessage,
    ReduceMessage,
    ReduceResponseMessage,
    ReduceOperator,
)
from scalp.node import Node
from scalp.node.computingcore import ComputingCore
from scalp.node import distribution
from .utils import euclidean_distance
from .payloads import InputVectorPayload, BMUPayload, InputLengthPayload


@dataclass
class KSomConfig:
    eps_start: float
    eps_end: float
    sigma_start: float
    sigma_end: float
    input_node: PositionVector
    inputs: Tuple[float, ...]
    iteration_cb: Callable[[int], None]


class KSom(ComputingCore):
    config: ClassVar[KSomConfig] = Optional[None]

    def __init__(
        self, node: Node, network_size: PositionVector, start_event: Event
    ) -> None:
        super().__init__(node, network_size, start_event)
        if not KSom.config:
            raise Exception("Please specify config at KSom.config")
        if not KSom.config.inputs:
            raise Exception("Please specify the inputs at KSom.config.inputs")

        self.__initialize_weigths(len(KSom.config.inputs[0]))
        self.__reduce_msgs: Dict[UUID, [Tuple[ReduceMessage, int]]] = {}
        self.__response_event = Event()
        self.__input_count = 0

    @property
    def weights(self):
        return self.__weights

    def __initialize_weigths(self, dimensions: int) -> None:
        self.__weights: List[float] = [self._node.position[0], self._node.position[1]]
        # self.__weights: List[float] = [
        #     self._rng.random_sample() * 8 for _ in range(dimensions)
        # ]

    def _execute(self) -> None:
        """
        Only the configured input node receives and broadcasts the input vectors
        """
        if self._node.position != self.config.input_node:
            return

        self.__send_input_length()
        for t, input_vector in enumerate(KSom.config.inputs):
            self.__emit_input_vector(t, input_vector)
            self.__response_event.wait()
            self.__response_event.clear()

            if self.config.iteration_cb:
                self.config.iteration_cb(t, input_vector)

    def __send_input_length(self) -> None:
        msg = BroadcastMessage(
            source=self._node.position,
            payload=InputLengthPayload(length=len(KSom.config.inputs)),
        )
        self.send_to_fpga(msg)

    def __emit_input_vector(self, t: int, input_vector: Tuple[float, ...]) -> None:
        msg = ReduceMessage(
            source=self._node.position,
            payload=InputVectorPayload(input_vector=input_vector),
            operator=ReduceOperator.MIN,
        )

        self.__reduce_msgs[msg.uuid] = [msg, t]
        self.send_to_fpga(msg)

    def __distance(self, input_vector: Tuple[float, ...]) -> float:
        return euclidean_distance(self.__weights, input_vector)

    def __update__weights(
        self, input_vector: List[float], bmu: PositionVector, t: int
    ) -> None:
        if not self.__input_count:
            raise Exception("Specify input_count first")

        # Compute epsilon and sigma
        eps = self.config.eps_start * pow(
            self.config.eps_end / self.config.eps_start, t / self.__input_count
        )
        sigma = self.config.sigma_start * pow(
            self.config.sigma_end / self.config.sigma_start, t / self.__input_count
        )

        dist_winner = euclidean_distance(self._node.position, bmu)

        # Compute neighbourhood function
        h_func = math.exp(-math.pow(dist_winner, 2) / (2.0 * pow(sigma, 2)))
        # Update weights
        for dim, value in enumerate(input_vector):
            self.__weights[dim] += eps * h_func * (value - self.__weights[dim])

    def handle_broadcast_msg(self, msg: BroadcastMessage) -> None:
        if isinstance(msg.payload, BMUPayload):
            self.__update__weights(
                msg.payload.input_vector, msg.payload.bmu_position, msg.payload.t
            )
            self.__response_event.set()
        elif isinstance(msg.payload, InputLengthPayload):
            self.__input_count = msg.payload.length
        else:
            raise Exception(f"Unknown payload type: {msg.payload}")

    def handle_reduce_msg(self, msg: ReduceMessage) -> Union[int, float]:
        if isinstance(msg.payload, InputVectorPayload):
            return self.__distance(msg.payload.input_vector)

        raise Exception(f"Unknown payload type: {msg.payload}")

    def handle_reduce_response_msg(
        self, reduce_response: ReduceResponseMessage
    ) -> None:
        """
        Handle messages received by ComputingCore
        """
        reduce_msg, t = self.__reduce_msgs[reduce_response.uuid]
        del self.__reduce_msgs[reduce_response.uuid]
        if isinstance(reduce_msg.payload, InputVectorPayload):
            bmu_msg = BroadcastMessage(
                source=self._node.position,
                payload=BMUPayload(
                    input_vector=reduce_msg.payload.input_vector,
                    bmu_position=reduce_response.source,
                    t=t,
                ),
            )
            self.send_to_fpga(bmu_msg)
        else:
            raise Exception(f"Unknown payload type: {reduce_msg.payload}")
