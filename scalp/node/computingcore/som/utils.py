from typing import Tuple, Sequence
import math


def euclidean_distance(p1: Sequence[float], p2: Sequence[float]) -> float:
    """Compute Euclidean distance from 2 points in the same space.

        :p1 point: First point in vector space.
        :p2 point: Secondpoint in vector space.
        :return: Distance to the point.
        :raise: AttributeError.
    """
    if len(p1) != len(p2):
        raise AttributeError(
            "Cannot calculate distance from elements with different dimensions"
        )
    dist_sum: float = 0
    for dim, value in enumerate(p1):
        dist_sum += pow(value - p2[dim], 2)
    return math.sqrt(dist_sum)
