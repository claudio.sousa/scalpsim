from typing import ClassVar, Union
from threading import Event
from time import sleep
from functools import reduce
from scalp import statistics
from scalp.common import (
    PositionVector,
    UnicastMessage,
    BroadcastMessage,
    ReduceMessage,
    ReduceResponseMessage,
)
from scalp.node import Node
from scalp.node.computingcore import ComputingCore
import numpy as np
import scalp.node.distribution as distribution


class GenerateBroadcastMessages(ComputingCore):
    MSGS_2_GENERATE: ClassVar[int] = 5
    finished_computingcores: ClassVar[int] = 0

    def __init__(
        self, node: Node, network_size: PositionVector, start_event: Event
    ) -> None:
        super().__init__(node, network_size, start_event)
        self._node_count = reduce(lambda a, c: a * c, network_size)
        self._expected_msgs = self.MSGS_2_GENERATE * self._node_count

    def _execute(self) -> None:
        for i in range(self.MSGS_2_GENERATE):
            msg = BroadcastMessage(
                source=self._node.position, payload=f"{i}@{self._node.position}"
            )

            self.send_to_fpga(msg)

    def handle_broadcast_msg(self, msg: BroadcastMessage)->None:
        """
        Handle broadcast messages received by ComputingCore
        """
        self._expected_msgs -= 1
        if self._expected_msgs == 0:
            GenerateBroadcastMessages.finished_computingcores += 1
            if self._node_count == GenerateBroadcastMessages.finished_computingcores:
                print(f"Finished ComputingCoreS: {GenerateBroadcastMessages.finished_computingcores}")
