from typing import ClassVar, Union
from threading import Event
from time import sleep
from functools import reduce
import numpy as np

from scalp import statistics
from scalp.common import (
    PositionVector,
    UnicastMessage,
    BroadcastMessage,
    ReduceMessage,
    ReduceResponseMessage,
    ReduceOperator,
)
from scalp.node import Node
from scalp.node.computingcore import ComputingCore
import scalp.node.distribution as distribution


class GenerateReduceMessages(ComputingCore):
    MSGS_2_GENERATE: ClassVar[int] = 5
    expected_msgs: ClassVar[int] = 0

    def __init__(
        self, node: Node, network_size: PositionVector, start_event: Event
    ) -> None:
        super().__init__(node, network_size, start_event)
        node_count = reduce(lambda a, c: a * c, network_size)
        GenerateReduceMessages.expected_msgs = (
            GenerateReduceMessages.MSGS_2_GENERATE * node_count
        )

    def _execute(self) -> None:
        for i in range(self.MSGS_2_GENERATE):
            msg = ReduceMessage(
                source=self._node.position, operator=ReduceOperator.SUM, payload=i + 1
            )

            self.send_to_fpga(msg)

    def handle_reduce_msg(self, msg: ReduceMessage) -> Union[int, float]:
        """
        Handle received reduce messages
        """
        return msg.payload

    def handle_reduce_response_msg(self, msg: ReduceResponseMessage) -> None:
        """
        Handle received reduce message responses
        """
        print(f"Received reduce response", msg.value)
        GenerateReduceMessages.expected_msgs -= 1
        if GenerateReduceMessages.expected_msgs == 0:
            print("All reduce messages were received")
