from typing import ClassVar, Union
from threading import Event
from time import sleep
import numpy as np
from functools import reduce
from scalp.common import PositionVector, UnicastMessage, BroadcastMessage, ReduceMessage, ReduceResponseMessage
from scalp.node import Node, distribution
from scalp import statistics
from scalp.node.computingcore import ComputingCore


class GenerateUnicastMessages(ComputingCore):
    INTERVAL: ClassVar[float] = 0.1
    DISTRIBUTION: ClassVar[distribution.distribution] = distribution.complement

    def __init__(
        self, node: Node, network_size: PositionVector, start_event: Event
    ) -> None:
        super().__init__(node, network_size, start_event)
        self._node_count = reduce(lambda a, c: a * c, network_size)

    def __pick_target_position(self):
        return GenerateUnicastMessages.DISTRIBUTION(
            source=self._node.position, network_size=self._network_size, rng=self._rng
        )

    def _execute(self) -> None:
        while True:
            target = self.__pick_target_position()
            msg = UnicastMessage(
                source=self._node.position,
                target=target,
                payload=f"{self._node.position}",
            )

            self.send_to_fpga(msg)

            if self.INTERVAL:
                sleep(self.INTERVAL)

    def handle_unicast_msg(self, msg: UnicastMessage) -> None:
        """
        Handle messages received by ComputingCore
        """
        statistics.received(self._node.position, self._node.clock.time, msg)
