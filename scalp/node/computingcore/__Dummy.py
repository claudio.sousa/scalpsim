from threading import Event
from scalp.common import PositionVector, Message, YIELD_EPSILON, Direction
from scalp import statistics
from scalp.node import Node
from .__ComputingCore import ComputingCore

class Dummy(ComputingCore):

    def _execute(self) -> None:
        pass
