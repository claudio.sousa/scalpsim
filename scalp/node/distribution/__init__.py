from typing import Callable
from functools import lru_cache
from itertools import product
import math
import numpy as np

from scalp.common import PositionVector

distribution = Callable[
    [PositionVector, PositionVector, np.random.RandomState], PositionVector
]


def uniform(
    source: PositionVector, network_size: PositionVector, rng: np.random.RandomState
) -> PositionVector:
    """
    Random target picked in the network such as the target is different from source
    """
    target_pos = source
    while target_pos == source:
        target_pos = PositionVector(
            *[rng.randint(0, dim_size) for dim_size in network_size]
        )
    return target_pos

@lru_cache(maxsize=1024)
def complement(
    source: PositionVector, network_size: PositionVector, rng: np.random.RandomState
) -> PositionVector:
    """
    Returns the complement for the sourcve
    Designed for networks where each dimension as a size of 2^k
    """
    return PositionVector(*[source[dim] ^ network_size[dim] - 1 for dim in range(3)])

@lru_cache(maxsize=1024)
def digitrevert(
    source: PositionVector, network_size: PositionVector, rng: np.random.RandomState
) -> PositionVector:
    """
    Reverts digits
    Designed for networks where each dimension as a size of 2^k
    """
    def revert(p:int, size:int)->int:
        return size if p == 0 else 0 if p == size else (p << 1 & size) + (p>>int(math.log2(size)))
    return PositionVector(*[revert(source[dim], network_size[dim] - 1) for dim in range(3)])



def sphere_of_locality(radius:float, p:float)->distribution:
    """
    Reverts digits
    Designed for networks where each dimension as a size of 2^k
    """
    @lru_cache(maxsize=1024)
    def choices_weights(source: PositionVector, network_size: PositionVector, rng: np.random.RandomState):
        positions = product(*[range(d) for d in network_size])
        inside = []
        outside = []
        for pos in positions:
            if pos == source:
                continue
            node_dist = sum((source[dim] - pos[dim])**2 for dim in range(3))**(1/2)
            if node_dist <= radius:
                inside.append(pos)
            else:
                outside.append(pos)

        return inside, outside

    def dist(source: PositionVector, network_size: PositionVector, rng: np.random.RandomState) -> PositionVector:
        inside, outside = choices_weights(source, network_size, rng)
        nodeset = inside if rng.random_sample() <= p else outside
        return nodeset[rng.randint(len(nodeset))]

    return dist

