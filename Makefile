ROUTER=xyz
DIST=uniform
RATE=1024
SEED=1

som:
	python som_simulator.py --inputs=10000 --plotlive=True

unicast:
	python stress_test.py --computingcore=unicast --router=$(ROUTER) --dist=$(DIST) --rate=$(RATE) --seed=$(SEED)

broadcast:
	python stress_test.py --computingcore=broadcast --router=$(ROUTER) --timeout=30 --seed=$(SEED)

reduce:
	python stress_test.py --computingcore=reduce --router=$(ROUTER) --timeout=30  --seed=$(SEED)

init:
	@python -m pip install -r requirements.txt

mypy:
# checks type decorations
	@mypy --ignore-missing-imports stress_test.py

pycycle:
# checks for circular dependencies
	@pycycle --here


.PHONY: init run mypy pycycle
