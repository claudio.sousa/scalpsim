from typing import List, NamedTuple
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np


def get_network_bandwidth(side: int) -> float:
    LINK_BANDWIDTH = 1 / 32
    CHANNELS_PER_LINK = 2
    nodes = side ** 3
    links = 2 * (3 * side ** 3 - 3 * side ** 2)
    bisection = (nodes ** (1 / 3)) ** 2
    bisection_bandwidth = bisection * CHANNELS_PER_LINK * LINK_BANDWIDTH
    prob_cross_bisection = 0.5
    max_network_bandwith = 1 / prob_cross_bisection * bisection_bandwidth
    return max_network_bandwith


FILENAME = "perf_{}_{}_{}.csv"

routers = {
    "XYZ": "xyz",
    "XYZ adaptive": "xyz_adapt",
    "Stochastic adaptive": "adaptive",
    "Virtual channels": "adaptive_vc",
}

distributions = {
    "Uniform": "uniform",
    "Digit Revert": "digitrevert",
    "Complement": "complement",
    "Sphere of locality": "sphere_of_locality",
}

columns = [
    "Rate",
    "Throughput",
    "Latency",
    "Peek latency",
    "Latency var",
    "Hops",
    "Messages",
    "Cycles",
    "Full events",
    "Time",
]


class Serie(NamedTuple):
    label: str
    router: str
    distribution: str
    side: int


def plot_charts(
    series: List[Serie],
    subtitle: str,
    normalize_throughput=True,
    peek_latency=True,
    accepted_workload=True,
) -> None:
    dfs = {}
    # average data by rate
    for serie in series:
        filename = FILENAME.format(serie.router, serie.distribution, serie.side)
        df = pd.read_csv(filename, names=columns)
        df = df.groupby(["Rate"], as_index=False).mean()
        if normalize_throughput:
            bandwidth = get_network_bandwidth(serie.side)
            df["Throughput"] /= bandwidth
        dfs[serie] = df

    throughput_label = (
        "Normalized " if normalize_throughput else ""
    ) + "Accepted Throughput"
    # plot latency vs throughput
    plt.figure()
    title = f"Latency vs Accepted Throughput ({subtitle})"
    plt.title(title)
    plt.xlabel(throughput_label)
    plt.ylabel("Average Latency (cycles)")
    for serie in dfs:
        df = dfs[serie]
        plt.plot(
            "Throughput",
            "Latency",
            data=df,
            label=serie.label,
            linestyle="-",
            marker="o",
        )
    plt.legend()
    plt.tight_layout()
    plt.grid()
    plt.savefig(f'img/{title}')

    if peek_latency:
        # plot peek latency vs throughput
        plt.figure()
        title = f"Peek latency vs Accepted Throughput ({subtitle})"
        plt.title(title)
        plt.xlabel(throughput_label)
        plt.ylabel("Peek Latency (cycles)")
        for serie in dfs:
            df = dfs[serie]
            plt.plot(
                "Throughput",
                "Peek latency",
                data=df,
                label=serie.label,
                linestyle="-",
                marker="o",
            )
        plt.legend()
        plt.tight_layout()
        plt.grid()
        plt.savefig(f'img/{title}')

    if accepted_workload:
        # plot latency vs throughput
        plt.figure()
        title = f"Accepted Throughput versus Applied Load ({subtitle})"
        plt.title(title)
        plt.xlabel("Applied Load (Messages / Second)")
        plt.ylabel(throughput_label)
        for serie in dfs:
            df = dfs[serie]
            plt.semilogx(
                "Rate",
                "Throughput",
                data=df,
                label=serie.label,
                linestyle="-",
                marker="o",
            )
        plt.legend()
        plt.tight_layout()
        plt.grid()
        plt.savefig(f'img/{title}')

for side in (4, ):
    for dist_title, dist in distributions.items():
        series = [
            Serie(label, router, dist, side) for (label, router) in routers.items()
        ]
        plot_charts(series, f"{dist_title}, {side**2} nodes")
        
for router_name, router in routers.items():
        # if router_name not in ("XYZ", ):
        #     continue
        series = [
            Serie(label, router, dist, 4)
            for (label, dist) in distributions.items()
        ]
        plot_charts(series, router_name)

for dist_title, dist in distributions.items():
    for router_name, router in routers.items():
        # if router_name != "XYZ":
        #     continue
        series = [Serie(f"{side**3} nodes", router, dist, side) for side in (4, 8)]
        plot_charts(
            series,
            f"{router_name}-{dist_title}",
            normalize_throughput=False,
            peek_latency=False,
            accepted_workload=False,
        )


plt.show()
