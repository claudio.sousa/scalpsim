#!/bin/bash
for side in 4, 8
do
    for dist in uniform complement digitrevert sphere_of_locality
    do
        for router in xyz xyz_adapt adaptive adaptive_vc
        do
            FILE=perf_${router}_${dist}_${side}.csv
            echo $FILE
            rm $FILE
            rate=32
            while [ $rate -le 16384 ]
            do
                for seed in `seq 1 1 30`
                do
                    python ../stress_test.py \
                                --computingcore=unicast \
                                --router=$router \
                                --dist=$dist \
                                --side=$side \
                                --rate=$rate \
                                --seed=$seed \
                                >> $FILE
                done
                rate=$(( $rate * 2 ))
            done
        done
    done
done